<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Validator;
use Illuminate\Support\Facades\Redirect;
use function GuzzleHttp\json_encode;

class ProductsController extends Controller
{

  public function view()
  {
    return view('Admin.ProductForm');
  }

  public function list(Request $request)
  {
    $totalCount = 0;
    if ($request['search']['value']) {
      $searchrow = Product::where([["english_name", "LIKE", '%' . $request['search']['value'] . '%'],["status","1"]])->orWhere([["arabic_name", "LIKE", '%' . $request['search']['value'] . '%'],["status","1"]])
        ->orWhere([["price", "LIKE", '%' . $request['search']['value'] . '%'],["status","1"]])
        ->orWhere([["dynamic_attributes", "LIKE", '%' . $request['search']['value'] . '%'],["status","1"]])
        ->skip($request['start'])->take($request['length'])->get();
      $products['data'] = $searchrow;
      $totalCount = Product::where("english_name", "LIKE", '%' . $request['search']['value'] . '%')->count();
    } else {
      $products['data'] = Product::skip($request['start'])->take($request['length'])->get();
      $totalCount = Product::count();
    }
    $products['draw'] = isset($request['draw']) ? $request['draw'] : 1;
    $products['recordsTotal'] = $totalCount;
    $products['recordsFiltered'] = $totalCount;
    echo json_encode($products);
  }


  public function add_new_Product(Request $request)
  {
    $returned_data = [];
    $validator = Validator::make($request->all(), [
      'arabic_name' => 'required',
      'english_name' => 'required',
      'price' => 'required',
      'status' => 'required',
    ]);

    if ($validator->fails()) {
      return redirect::back()->withErrors($validator->errors());
    } else if (!($validator->fails())) {

      $request['dynamic_attributes'] = (array) json_decode($request['dynamic_attributes']);
      $request['dynamic_attributes'] = $request['dynamic_attributes']['dynamic_attributes'];

      $Product = new Product();
      $Product->arabic_name = $request['arabic_name'];
      $Product->english_name = $request['english_name'];
      $Product->price = $request['price'];
      $Product->status = $request['status'];
      $Product->main_img = $request->file('main_img')==null?null:$request->file('main_img')->getClientOriginalName();
      $Product->gallery_img = $request->file('gallery_img')==null?null:$request->file('gallery_img')->getClientOriginalName();
      $Product->dynamic_attributes = $request['dynamic_attributes'];
      $Product->save();

      $returned_data['code'] = 1;
      $returned_data['data'] = $request;
      $returned_data['msg'] = __('errors.success');

      return json_encode($returned_data);
    } else {
      $returned_data['code'] = -100;
      $returned_data['data'] = $request;
      $returned_data['msg'] = __('errors.error');
    }


  }


}
