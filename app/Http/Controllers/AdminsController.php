<?php

namespace App\Http\Controllers\Auth;

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Auth;

class AdminsController extends Controller
{

  protected $user;

  public function __construct()
  {
    $this->middleware(function ($request, $next) {

      $this->user = Auth::user();

      return $next($request);
    });
  }


  public function login(Request $request)
  {
    $returned_data = [];
    $admin_search = Admin::where('email', "LIKE", '%' . $request['email'] . '%')->first();

    if (!empty($admin_search) || $admin_search != null) {
      if (Hash::check($request['password'], $admin_search->password)) {
        $admin_search->remember_token = Str::random(50);
        $admin_search->last_login = Carbon::now();

        $admin_search->save();
        Session::put('user_remember_token', $admin_search->remember_token);
        Session::put('remember_token', $admin_search->remember_token);
        Session::put('admin_name', $admin_search->name);
        Session::put('admin_id', $admin_search->id);;
        $returned_data['code'] = 1;
        $returned_data['msg'] = __('errors.success');
      } else {
        $returned_data['code'] = -200;
        $returned_data['msg'] = 'Password is not correct';
      }
    } else {
      $returned_data['code'] = -100;
      $returned_data['msg'] = 'User is not found';
    }

    return json_encode($returned_data);

  }

  public function logout(Request $request)
  {
    $user = Admin::find(Session::get('admin_id'));
    $user->remember_token = null;
    $user->save();
    Session::flush();
    return redirect('/admin/login');
  }



}
