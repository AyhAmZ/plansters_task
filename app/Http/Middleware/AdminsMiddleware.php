<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use URL;

class AdminsMiddleware
{

  public function handle($request, Closure $next)
  {
    if(URL::current() == 'http://localhost:8000/user/userpage')
    {
      return $next($request);
    }
    if (Session::get('remember_token') != null) {
      if (URL::current() == 'http://localhost:8000/admin/login') {
        return redirect('/');
      }
      return $next($request);
    } else {
      return response()->view('Admin.Auth.Login');

    }
  }
}
