<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $table = 'products';
  protected $fillable = [
    'arabic_name', 'english_name', 'price', 'status', 'main_img', 'gallery_img', 'dynamic_attributes'
  ];

  protected $casts = [
    'dynamic_attributes' => 'array',
  ];

}
