<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});


Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
  Route::post('/login', 'AdminsController@login');
  Route::get('/logout', 'AdminsController@logout');
  Route::get('/products', 'ProductsController@list');
  Route::post('/AddProduct', 'ProductsController@add_new_Product');

});


Route::group(['prefix' => 'user', 'as' => 'user'], function () {
  Route::get('/products', 'ProductsController@list');
});


