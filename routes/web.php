<?php


Route::get('/sk-layout-2-columns', 'StaterkitController@columns_2');
Route::get('/sk-layout-fixed-navbar', 'StaterkitController@fixed_navbar');
Route::get('/sk-layout-floating-navbar', 'StaterkitController@floating_navbar');
Route::get('/sk-layout-fixed', 'StaterkitController@fixed_layout');


Route::get('/', 'DashboardController@dashboardAnalytics');
Route::get('/dashboard-analytics', 'DashboardController@dashboardAnalytics');


Route::group(['prefix' => 'user', 'as' => 'user.', ['middleware' => ['Users']]], function () {
  Route::get('/userpage', function () {
    return view('User.UserPage');
  });

});

Route::group(['prefix' => 'admin', 'as' => 'admin.', ['middleware' => ['Admins']]], function () {
  Route::view('/login', 'Admin.Auth.Login')->name('login');
  Route::view('/product', 'Admin.products-list')->name('admin/products');
  Route::get('/ProductForm', 'ProductsController@view');

});





//factory(App\Admin::class, 3)->create();

