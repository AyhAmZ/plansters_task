<?php

return [

    'Quiz_Form' => 'إستمارة الإختبار',
    'Quiz_ID' => 'رقم الإختبار',
    'Name' => 'إسم الإختبار',
    'Total_Time' => 'الوقت الكلي للإختبار',
    'Total_Questions' => 'عدد أسئلة الإختبار',
    'Actions' => 'أفعال',
    'Add_New_Quiz' => 'إضافة إختبار جديد',
    'Quiz_list_title' => 'قائمة الإختبارات',
    'Number_questions_per_topic' => 'عدد الأسئلة بكل موضوع',
];
