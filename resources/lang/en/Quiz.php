<?php

return [

    'Quiz_Form' => 'Quiz Form',
    'Quiz_ID' => 'Quiz ID',
    'Name' => 'Quiz Name',
    'Total_Time' => 'Quiz Total Time',
    'Total_Questions' => 'Quiz Total Questions',
    'Actions' => 'Actions',
    'Add_New_Quiz' => 'Add New Quiz',
    'Quiz_list_title' => 'Quizzes List',
    'Number_questions_per_topic' => 'Number Questions Per topic',
];
