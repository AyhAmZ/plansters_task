@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')


@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
@endsection


@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/plugins/extensions/toastr.css')) }}">

@endsection
@section('content')
    <div class="d-flex justify-content-center">
        <div class="card bg-authentication rounded-0 mb-0">
            <div class="row m-0">
                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                    <img src="{{ asset('images/pages/login.png') }}" alt="branding logo">
                </div>
                <div class="col-lg-6 col-12 p-0">
                    <div class="card rounded-0 mb-0 px-2">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Login</h4>
                            </div>
                        </div>
                        <p class="px-2">Welcome to admin panel</p>
                        <div class="card-content">
                            <div class="card-body pt-1">
                                <form class="loginform" type="POST" data-parsley-validate="">
                                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                                        <input type="text" class="form-control" id="email" name="email"
                                               placeholder="Email" required
                                               data-parsley-required-message="User Name field is required">
                                        <div class="form-control-position">
                                            <i class="feather icon-user"></i>
                                        </div>
                                        <label for="email">Email</label>
                                    </fieldset>

                                    <fieldset class="form-label-group position-relative has-icon-left">
                                        <input type="password" class="form-control" id="user_password" name="password"
                                               placeholder="Password" required
                                               data-parsley-required-message="Password field is required">
                                        <div class="form-control-position">
                                            <i class="feather icon-lock"></i>
                                        </div>
                                        <label for="user-password">Password</label>
                                    </fieldset>
                                    <div class="form-group d-flex justify-content-between align-items-center">
                                        <button type="submit" class="btn btn-primary float-right btn-inline">Login
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="login-footer">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset(mix('js/scripts/ui/parsley.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/toastr.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/ui/Login.js')) }}"></script>
@endsection
