@extends('layouts/contentLayoutMaster')

@section('title','Add Product')

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
@endsection
@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/extensions/toastr.css')) }}">
@endsection

@section('content')
  <style>
    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
      border-color: #48B04B !important;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
      border-color: #D50000 !important;
    }

    .parsley-errors-list {
      margin: 10px 0 0 0;
      padding: 0;
      list-style-type: none;
      font-size: 0.9em;
      line-height: 0.9em;
      opacity: 0;
      color: #D50000;
      transition: all .3s ease-in;
      -o-transition: all .3s ease-in;
      -moz-transition: all .3s ease-in;
      -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list li {
      line-height: 1.4;
    }

    .parsley-errors-list.filled {
      opacity: 1;
    }

    .sel2 .parsley-errors-list.filled {
      margin-top: 46px;
      margin-bottom: -64px;
    }

    .sel2 .parsley-errors-list:not(.filled) {
      display: none;
    }

    .sel2 .parsley-errors-list.filled + span.select2 {
      margin-bottom: 30px;
    }

    .sel2 .parsley-errors-list.filled + span.select2 span.select2-selection--single, .sel2 .parsley-errors-list.filled + span.select2 span.select2-selection--multiple {
      border: 1px solid #D50000;
    }

  </style>

  <section id="multiple-column-form">
    <div class="row match-height">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Add Product</h4>
          </div>
          <div class="card-content">
            <div class="card-body">
              <form class="form" data-parsley-validate="">
                {{ csrf_field() }}
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-label-group ">
                        <span>Arabic Name</span>
                        {{ csrf_field() }}
                        <input type="text" required
                               data-parsley-required-message="Arabic Name field is required"
                               id="arabic_name" class="form-control"
                               placeholder="Arabic Name" name="arabic_name">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-label-group ">
                        <span>English Name</span>
                        {{ csrf_field() }}
                        <input type="text" required
                               data-parsley-required-message="English Name field is required"
                               id="english_name" class="form-control"
                               placeholder="English Name" name="english_name">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-label-group">
                        <span>Price</span>
                        {{ csrf_field() }}
                        <input type="number" required
                               id="price" class="form-control"
                               placeholder="Price" name="price"
                               data-parsley-required-message=" Price field is required">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-label-group">
                        <span>Status</span>
                        {{ csrf_field() }}
                        <div>
                          <select class="form-control" name="status" id="status">
                            <option value="1">Active</option>
                            <option value="0">Not Active</option>
                          </select>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                        <fieldset class="form-group">
                          <label for="basicInputFile">Main Image</label>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="main_img" name="main_img">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                          </div>
                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset class="form-group">
                          <label for="basicInputFile">Gellery Image</label>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="gallery_img" name="gallery_img">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                          </div>
                        </fieldset>
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-11">
                      <div data-repeater-list="dynamic_attributes" class="dynamic_attributes">
                        <div class="row" data-repeater-item >
                          <div class="col-md-6">
                            <span>Attribute key</span>
                            <div class="form-label-group">
                              <input type="text" class="form-control nq"
                                     name="attribute_key"
                                     placeholder="Attribute Key"
                              >
                            </div>
                          </div>

                          <div class="col-md-5">
                            <span>Attribute Value</span>
                            <div class="form-label-group">
                              <input type="text" class="form-control nq"
                                     name="attribute_value"
                                     placeholder="Attribute Value"
                              >
                            </div>
                          </div>
                          <div class="col-md-1">
                            <button data-repeater-delete class="btn btn-danger mt-1"
                                    type="button">
                              <i class="fa fa-times-circle"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-1">
                      <button data-repeater-create class="btn btn-dark mt-1"
                              id="data-repeaterrr"
                              type="button">
                        <i class="fa fa-plus-circle"></i>
                      </button>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="add-data-btn">
                        <input id="submit_button"
                               data-id="" type="submit"
                               class="btn btn-primary" value="Submit">

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


@endsection

@section('vendor-script')
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>

  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>

@endsection

@section('page-script')
  <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/ui/parsley.min.js')) }}"></script>

  <script src="{{ asset(mix('js/scripts/extensions/toastr.js')) }}"></script>
  <script src="{{asset(mix('js/scripts/ui/ProductForm.js'))}}"></script>


@endsection
