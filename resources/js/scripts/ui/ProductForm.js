$(document).ready(function () {
  "use strict"
  var topics = null;


  $('.form').repeater({
    show: function () {

      $(this).slideDown();
    },
    hide: function (deleteElement) {
      if (confirm('Are you sure you want to delete this element?')) {
        $(this).slideUp(deleteElement);
      }
    },
    isFirstItemUndeletable: true,
  });


  $(document).on('submit', '.form', function (e) {
    let formData = new FormData();
    formData.append('main_img',$("#main_img")[0].files[0]);
    formData.append('gallery_img',$("#gallery_img")[0].files[0]);
    formData.append('arabic_name',$("#arabic_name").val());
    formData.append('english_name',$("#english_name").val());
    formData.append('price',$("#price").val());
    formData.append('status',$("#status").val());
    formData.append('dynamic_attributes',JSON.stringify($(".dynamic_attributes").repeaterVal()));

    if ($('.form').parsley().isValid()) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: "/api/admin/AddProduct",
        dataType: 'json',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          if (data['code'] === 1) {
            toastr.success('Product added successfully', 'Great!');
            window.location = '/admin/product';
          } else if (data['code'] === -100) {
            toastr.success('Something was wrong ! ', 'Warning!');
            window.location = '/admin/ProductForm';
          }
        }
      });



    }
  });



});
