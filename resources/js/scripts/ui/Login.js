$(document).ready(function () {


  $(document).on('submit', '.loginform', function (e) {
    e.preventDefault();
    if ($('.loginform').parsley().isValid()) {

      $.ajax({
        type: 'POST',
        url: "/api/admin/login",
        dataType: 'json',
        data: $('.loginform').serialize(),
        success: function (data) {
          // console.log(data);

          if (data['code'] === 1) {
            toastr.success('Great you are logged in ! ', 'Successs!');
            window.location = '/';
          } else if (data['code'] === -200) {
            toastr.warning('Password Is Not Correct! ', '');
          } else if (data['code'] === -100) {
            toastr.warning('User Is not found try again please! ', '');
          }
        }
      });

    }


  });


});
