
$(document).ready(function () {
  "use strict"

  var iii = 0;
  var table = $("#products-table");
  var response_global;
  var rrr;
  var dataListView = $(".data-list-view").DataTable({


    ajax: {
      url: "http://localhost:8000/api/user/products",
      type: "GET",
    },
    searchDelay: 1000,
    PaginationType: "full_numbers",
    Searchable: true,
    Filter: true,
    LengthChange: false,
    processing: true,
    serverSide: true,
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: {selectRow: true}
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    columns: [
      {data: "id"},
      {data: "arabic_name"},
      {data: "english_name"},
      {
        data: "price",
        render: function (data) {
          return data + " SYP";
        }
      },
      {data: "status"},
      {
        data: "main_img",
        render: function (data, type, row, meta) {
          if (data === null) {
            return '<img src="/images/homepod.png" height="60" width="60"/>';
          } else {
            return '<img src="/images/' + data + '" height="60" width="60"/>';
          }
        }
      },
      {
        data: "gallery_img",
        render: function (data, type, row, meta) {
          if (data === null) {
            return '<img src="/images/homepod.png" height="60" width="60"/>';
          } else {
            return '<img src="/images/' + data + '" height="60" width="60"/>';
          }
        }
      },
      {
        data: "dynamic_attributes",
        render: function (data) {
          return JSON.stringify(data);
        }
      },
    ],

    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        action: function () {
          $(this).removeClass("btn-secondary")
          $(".add-new-data").addClass("show")
          $(".overlay-bg").addClass("show")
          $("#data-name, #data-price").val("")
          $("#data-category, #data-status").prop("selectedIndex", 0)
        },
      }
    ],
    initComplete: function (settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  });


  dataListView.on('draw.dt', function () {
    setTimeout(function () {
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });


  // init thumb view datatable
  var dataThumbView = $(".data-thumb-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: {selectRow: true}
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        action: function () {
          $(this).removeClass("btn-secondary")
          $(".add-new-data").addClass("show")
          $(".overlay-bg").addClass("show")
          $("#data-name, #data-price").val("")
          $("#data-category, #data-status").prop("selectedIndex", 0)
        },
      }
    ],
    initComplete: function (settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  })

  dataThumbView.on('draw.dt', function () {
    setTimeout(function () {
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  var actionDropdown = $(".actions-dropodown")
  actionDropdown.insertBefore($(".top .actions .dt-buttons"))


  if ($(".data-items").length > 0) {
    new PerfectScrollbar(".data-items", {wheelPropagation: false})
  }


  $(document).on("click", ".hide-data-sidebar, .cancel-data-btn, .overlay-bg", function () {
    $(".add-new-data").removeClass("show")
    $(".overlay-bg").removeClass("show")
    $("#data-name, #data-price").val("")
    $("#data-category, #data-status").prop("selectedIndex", 0)
  });

  $(document).on("click", '.action-edit', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var data_id = $(this).attr("data-id");
    window.location = '/admin/ProductForm/' + data_id;
  });


});




