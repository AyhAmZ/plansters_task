/*=========================================================================================
    File Name: data-list-view.js
    Description: List View
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function () {
  "use strict"



  var iii = 0;
  var table = $("#products-table");
  var response_global;
  var rrr;
  var dataListView = $(".data-list-view").DataTable({

    ajax: {
      url: "http://localhost:8000/api/admin/products",
      type: "GET",
    },
    searchDelay: 1000,
    PaginationType: "full_numbers",
    Searchable: true,
    Filter: true,
    LengthChange: false,
    processing: true,
    serverSide: true,
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: {selectRow: true}
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    columns: [
      {data: "id"},
      {data: "arabic_name"},
      {data: "english_name"},
      {
        data: "price",
        render: function (data) {
          return data + " SYP";
        }
      },
      {data: "status"},
      {
        data: "main_img",
        render: function (data, type, row, meta) {
          if (data === null) {
            return '<img src="/images/homepod.png" height="60" width="60"/>';
          } else {
            return '<img src="/images/' + data + '" height="60" width="60"/>';
          }
        }
      },
      {
        data: "gallery_img",
        render: function (data, type, row, meta) {
          if (data === null) {
            return '<img src="/images/homepod.png" height="60" width="60"/>';
          } else {
            return '<img src="/images/' + data + '" height="60" width="60"/>';
          }
        }
      },
      {
        data: "dynamic_attributes",
        render: function (data) {
          return JSON.stringify(data);
        }
      },
    ],

    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        text: "<i href='/admin/ProductForm' class='feather icon-plus'></i> Add New Product",
        action: function () {
          $(this).removeClass("btn-secondary")
          $(".add-new-data").addClass("show")
          $(".overlay-bg").addClass("show")
          $("#data-name, #data-price").val("")
          $("#data-category, #data-status").prop("selectedIndex", 0)
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function (settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  });

  dataListView.on('draw.dt', function () {
    setTimeout(function () {
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });


  // init thumb view datatable
  var dataThumbView = $(".data-thumb-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: {selectRow: true}
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        text: "<i href='/admin/ProductForm' class='feather icon-plus'></i> Add New Product",
        action: function () {
          $(this).removeClass("btn-secondary")
          $(".add-new-data").addClass("show")
          $(".overlay-bg").addClass("show")
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function (settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  })

  dataThumbView.on('draw.dt', function () {
    setTimeout(function () {
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  // To append actions dropdown before add new button
  var actionDropdown = $(".actions-dropodown")
  actionDropdown.insertBefore($(".top .actions .dt-buttons"))


  // Scrollbar
  if ($(".data-items").length > 0) {
    new PerfectScrollbar(".data-items", {wheelPropagation: false})
  }


  // Close sidebar
  $(document).on("click", ".hide-data-sidebar, .cancel-data-btn, .overlay-bg", function () {
    $(".add-new-data").removeClass("show")
    $(".overlay-bg").removeClass("show")
    $("#data-name, #data-price").val("")
    $("#data-category, #data-status").prop("selectedIndex", 0)
  });

  $(document).on("click", '.action-edit', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var data_id = $(this).attr("data-id");
    window.location = '/admin/ProductForm/' + data_id;
  });

  /*
    $('.dataTables_filter').on('keyup', function () {
      $.ajax({
        url: "http://localhost:8000/api/admin/products",
        type: "GET",
      });
    });*/

  // $(document).on("click", '.action-edit', function (e) {
  //     e.preventDefault();
  //     e.stopPropagation();
  //     document.querySelector('#hfiled').value=2;
  //     var data_id = $(this).attr("data-id");
  //     var mydata;
  //     $.ajax({
  //         type: 'GET',
  //         url: "/api/admin/topic/" + data_id,
  //         dataType: 'json',
  //         success: function (data) {
  //             // console.log("submission was successful");
  //             //console.log(data);
  //             mydata = data["name"];
  //
  //             $('#data-name').val(mydata);
  //             $(".add-new-data").addClass("show");
  //             $(".overlay-bg").addClass("show");
  //         },
  //         error: function (data) {
  //             console.log("Error");
  //             console.log(data);
  //         }
  //     });
  //
  // });
  //
  //
  // // On Delete

  $(document).on("click", '.action-delete', function (e) {
    e.stopPropagation();
    var data_id = $(this).attr("data-id");
    toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn btn-warning px-1 py-1'>Yes</button>", 'delete ?',
      {
        closeButton: true,

        onShown: function (toast) {
          $("#confirmationRevertYes").click(function () {
            $.ajax({
              type: 'DELETE',
              url: "/api/admin/quiz/" + data_id,
              dataType: 'json',
              success: function (data) {
                dataListView.ajax.reload();
                toastr.success('Quiz was deleted successfully ! ', 'Great!');
                $(this).closest('td').parent('tr').fadeOut();
              },
              error: function (data) {
                // toastr.success('Something was wrong ! ', 'Warning!');
              }
            });

          });
        }
      });
  });

  // mac chrome checkbox fix
  if (navigator.userAgent.indexOf("Mac OS X") != -1) {
    $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
  }

  $(document).on("click", '.btn-outline-primary', function (e) {
    e.stopPropagation();
    window.location = '/admin/ProductForm';
  });


  // var addtopicfrm = $('#addfrm');
  // addtopicfrm.submit(function (e) {
  //     if(document.querySelector('#hfiled').value==="1")
  //     {
  //         // console.log('1');
  //         e.preventDefault();
  //         $.ajax({
  //             type: 'POST',
  //             url: "/api/admin/addTopic",
  //             dataType: 'json',
  //             data: addtopicfrm.serialize(),
  //             success: function (data) {
  //                 // console.log("submission was successful");
  //                 // console.log(data);
  //                 // console.log(data['code']);
  //                 if (data['code'] === 1) {
  //                     //console.log($("#topics-table").val())
  //                     dataListView.ajax.reload()
  //                     $(".add-new-data").removeClass("show")
  //                     $(".overlay-bg").removeClass("show")
  //                     $("#data-name, #data-price").val("")
  //                     $("#data-category, #data-status").prop("selectedIndex", 0)
  //                     $('.toast').prependTo('.toast-bs-container .toast-position').toast('show')
  //                 }
  //
  //             },
  //             error: function (data) {
  //                 $('.toast2').prependTo('.toast-bs-container2 .toast-position2').toast('show')
  //
  //             }
  //         });
  //     }else if(document.querySelector('#hfiled').value==="2")
  //     {
  //         // console.log('2');
  //         e.preventDefault();
  //         var data_id = $('.action-edit').attr("data-id");
  //         $.ajax({
  //             type: 'POST',
  //             url: "/api/admin/topic/"+data_id,
  //             dataType: 'json',
  //             data: addtopicfrm.serialize(),
  //             success: function (data) {
  //                 if (data['code'] === 1) {
  //                     dataListView.ajax.reload()
  //                     $(".add-new-data").removeClass("show")
  //                     $(".overlay-bg").removeClass("show")
  //                     $("#data-name, #data-price").val("")
  //                     $("#data-category, #data-status").prop("selectedIndex", 0)
  //                     $('.toast').prependTo('.toast-bs-container .toast-position').toast('show')
  //                 }
  //
  //             },
  //             error: function (data) {
  //                 $('.toast2').prependTo('.toast-bs-container2 .toast-position2').toast('show')
  //
  //             }
  //         });
  //     }
  //
  // });


});




