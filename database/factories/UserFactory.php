<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;
use App\Admin;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'id' => $faker->numberBetween(1, 100),
        'name' => $faker->name,
        'email' =>  $faker->email,
        'password' => Hash::make('123123123'),
        'last_login' => Carbon::now(),
        'role' => '0',
        'active' => '1',
    ];
});
